// Viết chương trình nhập vào thông tin của 1 cá nhân (Họ tên, tổng thu nhập năm, số
//   người phụ thuộc). Tính và xuất thuế thu nhập cá nhân phải trả theo quy định sau:
//   ❖ Thu nhập chịu thuế = Tổng thu nhập năm - 4tr- Số người phụ thuộc * 1.6tr
//   Thu nhập chịu thuế(triệu)           Thuế suất (%)
//   Đến 60                                   5
//   Trên 60 đến 120                          10
//   Trên 120 đến 210                         15
//   Trên 210 đến 384                         20
//   Trên 384 đến 624                         25
//   Trên 624 đến 960                         30
//   Trên 960                                 35

function TinhTien() {
  var hoVaTen = document.getElementById("hoten").value;
  var tongThuNhapTrenNam = document.getElementById("tongthunhap").value;
  var soNguoiPhuThuoc = document.getElementById("songuoi").value;
  var tRieu = 1000000;
  var thuNhapChiuThue =
    tongThuNhapTrenNam - 4 * tRieu - soNguoiPhuThuoc * 1.6 * tRieu;
  if (thuNhapChiuThue <= 20000) {
    alert("Không hợp lệ");
  }

  var tienThue = 0;
  if (thuNhapChiuThue <= 60 * tRieu) {
    tienThue = (thuNhapChiuThue * 5) / 100;
  } else if (60 * tRieu <= thuNhapChiuThue && thuNhapChiuThue <= 120 * tRieu) {
    tienThue = (thuNhapChiuThue * 10) / 100;
  } else if (120 * tRieu <= thuNhapChiuThue && thuNhapChiuThue <= 210 * tRieu) {
    tienThue = (thuNhapChiuThue * 15) / 100;
  } else if (210 * tRieu <= thuNhapChiuThue && thuNhapChiuThue <= 384 * tRieu) {
    tienThue = (thuNhapChiuThue * 20) / 100;
  } else if (384 * tRieu <= thuNhapChiuThue && thuNhapChiuThue <= 624 * tRieu) {
    tienThue = (thuNhapChiuThue * 25) / 100;
  } else if (624 * tRieu <= thuNhapChiuThue && thuNhapChiuThue <= 960 * tRieu) {
    tienThue = (thuNhapChiuThue * 30) / 100;
  } else {
    tienThue = (thuNhapChiuThue * 35) / 100;
  }
  document.getElementById(
    "result"
  ).innerHTML = `Họ Và Tên: ${hoVaTen} </br>  Tiền thuế: ${tienThue}VND`;
}
